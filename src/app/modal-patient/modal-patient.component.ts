import { Component, OnInit, ViewChild } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { Color, Label } from 'ng2-charts';
import { ChartOptions, ChartDataSets } from 'chart.js';
import * as _ from 'lodash';
import * as moment from 'moment';

@Component({
  selector: 'app-modal-patient',
  templateUrl: './modal-patient.component.html',
  styles: [
  ]
})
export class ModalPatientComponent implements OnInit {
  @ViewChild('content', { static: true }) private content: any;

  pagePatient = 1;

  provinceName: any;
  data: any = [];

  male = 0;
  female = 0;
  unknow = 0;

  death = 0;

  thai = 0;

  public lineChartData: ChartDataSets[] = [
    {
      data: [],
      label: 'ผู้ป่วยรายใหม่',
      fill: true,
    }
  ];
  public lineChartLabels: Label[] = [];
  public lineChartOptions: ChartOptions = {
    responsive: true,
    scales: {
      yAxes: [{
        gridLines: {
          drawBorder: false
        },
        scaleLabel: {
          display: true,
          labelString: 'จำนวนผู้ป่วยสะสม (ราย)'
        }
      }],
      xAxes: [{
        type: 'time',
        time: {
          unit: 'day'
        },
        distribution: 'series',
        offset: true,
        ticks: {
          major: {
            enabled: true,
            fontStyle: 'bold'
          },
          source: 'data',
          autoSkip: true,
          autoSkipPadding: 75,
          maxRotation: 0,
          sampleSize: 100
        },
      }]
    }
  };
  public lineChartColors: Color[] = [];
  public lineChartLegend = false;
  public lineChartType = 'line';
  public lineChartPlugins = [];

  constructor(private modalService: NgbModal) { }

  ngOnInit(): void {
  }

  open(provinceName: any, data: any) {

    this.pagePatient = 1;
    this.data = [];

    this.provinceName = provinceName;
    this.data = data.map(v => {
      v.ConfirmDate = moment(v.ConfirmDate).format('YYYY-MM-DD');
      v.thConfirmDate = moment(v.ConfirmDate).locale('th').format('D MMM YYYY');
      return v;
    });

    const group = _.groupBy(data, 'ConfirmDate');
    this.lineChartData[0].data = [];
    this.lineChartLabels = [];

    _.forEach(group, (v: any, k: any) => {
      this.lineChartLabels.push(moment(k).format('YYYY-MM-DD'));
      this.lineChartData[0].data.push(v.length);
    });

    this.male = 0;
    this.female = 0;
    this.unknow = 0;
    this.thai = 0;

    data.forEach((v: any) => {
      if (v.GenderEn === 'Male') this.male++;
      if (v.GenderEn === 'Female') this.female++;
      if (v.GenderEn === 'Unknow') this.unknow++;
      if (v.NationEn === 'Thai') this.thai++;
    });

    this.modalService
      .open(this.content, {
        ariaLabelledBy: 'modal-title',
        centered: true,
        backdrop: 'static',
        size: 'xl',
        scrollable: true
      })
      .result
      .then((result) => { }, (reason) => { });



  }


}
